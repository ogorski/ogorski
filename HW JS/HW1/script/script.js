'use strict';

let nameUser = prompt('What is your name?', ''),
    ageUser = prompt('How old are you?', '');

while (nameUser === '') {
    nameUser = prompt('What is your name?', nameUser);
}

while (ageUser === '' || isNaN(ageUser) || ageUser == null) {
    ageUser = prompt('How old are you?', ageUser);
}

if (ageUser < 18) {
    alert('You are not allowed to visit this website');
    window.stop();
} else if (ageUser <= 22) {
    let answer = confirm('Are you sure you want to continue?');

    if (answer === true) {
    alert(`Welcome, ${nameUser}.`);
    } else {
    alert('You are not allowed to visit this website');
    window.stop();
    }
} else {
    alert(`Welcome, ${nameUser}.`);
}
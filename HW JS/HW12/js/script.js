//HTML section
const images = document.getElementsByClassName('image-to-show');
const btnStop = document.getElementsByClassName('btn-stop')[0];
const btnContinue = document.getElementsByClassName('btn-continue')[0];
const timer = document.getElementsByClassName('timer')[0];


//Help variables
let slideIndex = 1;
let time = 2000;
let action;
let interval;


//HTML text
timer.innerHTML = `Image will be turned over after: ${Math.floor(time / 1000)} sec ${Math.round(time % 1000)} ms`;


//Helpers
let showSlide = (n) => {
    if (n > images.length) slideIndex = 1;
    [...images].forEach(item => item.style.display = 'none');
    images[slideIndex - 1].style.display = 'block';
};

let nextSlide = (n) => {
    showSlide(slideIndex += n);
};

let showTimer = (ms) => {
    interval = setInterval(() => {
        timer.innerHTML = `Page will be cleaned after: ${Math.floor(ms-- / 1000)} sec ${Math.round(ms-- % 1000)} ms`;
        if (ms === 0) {
            clearInterval(interval);
            showTimer(time);
        }
    }, 1);
};

let sliderInterval = () => {
    action = setInterval(() => {
        nextSlide(1)
    }, time);
};


//Events section
btnStop.addEventListener('click', () => {
    clearInterval(action);
    clearInterval(interval);
});

btnContinue.addEventListener('click', () => {
    clearInterval(interval);
    showTimer(time);
    clearInterval(action);
    btnContinue.innerHTML = "Возобновить показ";
    sliderInterval();
});


//Start page
showSlide(slideIndex);


let number = prompt('Enter some number', '');

while (number === '' || isNaN(number) || number == null || (number % 1 !== 0)) {
    number = prompt('You entered not a integer number. Please, enter some number again', number);
}

let enteredNumber = +number,
    resultNumbers = [];

for (let i = 0; i <= enteredNumber; i++) {
    if (i % 5 === 0 && i !== 0) {
        resultNumbers.push(i);
    }
}

if (resultNumbers && resultNumbers.length) {
    console.log(String(resultNumbers));
} else {
    console.log('Sorry, there are no numbers here');
}



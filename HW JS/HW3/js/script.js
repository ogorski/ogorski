const getNumbers = () => {
    let numbers = [];

    for (let i = 0; i < 2; i++) {
        numbers[i] = prompt('Please enter number here', '');

        while (numbers[i] === '' || isNaN(numbers[i]) || numbers[i] == null) {
            numbers[i] = prompt('You have entered not a number. Please enter correct number', numbers[i]);
        }
    }
    return numbers;
};


// Получение математической операции 

const getOperation = () => {
    let mathOperation = prompt('Please enter math operation ("+", "-", "*", "/")', '+');
    while (mathOperation !== '+' && mathOperation !== '-' && mathOperation !== '*' && mathOperation !== '/') {
        mathOperation = prompt('You have entered wrong operation. Please enter operation like "+", "-", "*", "/")', '+');
    }
    return mathOperation;
};


// Окончательный результат 

const showResult = (dataNumbers, operation) => {
    let result = dataNumbers;
    result.splice(1, 0, operation);
    return eval(result.join(' '));
};

console.log(showResult(getNumbers(),getOperation()));

"use strict"

const createNewUser = () => {
    let newUser = {
        set userFirstName(name) {
            this.FirstName = name;
        },
        set userLastName(newValue) {
            this.LastName = newValue;
        },
        set birthdayUser(birthd) {
            this.birthday = birthd;
        },
        getLogin() {
            return (this.FirstName[0] + this.LastName).toLowerCase()
        },
        getAge() {
            let dayOfBirthday = Number(this.birthday.substring(0,2));
            let monthOfBirthday = Number(this.birthday.substring(3,5));
            let yearOfBirthday = Number(this.birthday.substring(6,10));
            let birthdayDate = new Date(yearOfBirthday, monthOfBirthday-1, dayOfBirthday);
            let nowDate = new Date();
            let age = nowDate.getFullYear() - birthdayDate.getFullYear();
            let monthAge = nowDate.getMonth() - birthdayDate.getMonth();
            if (monthAge < 0 || (monthAge === 0 && nowDate.getDate() < birthdayDate.getDate())) {
                age = age - 1;
            }
            return age

        },
        getPassword() {
            return this.FirstName[0].toUpperCase() + this.LastName.toLowerCase() + this.birthday.substring(6,10)
        }
    };

    Object.defineProperty(newUser, 'First Name', {
        value: newUser.userFirstName = setFirstName(),
        writable: false,
        configurable: false
    });
    Object.defineProperty(newUser, 'Last Name', {
        value: newUser.userLastName = setLastName(),
        writable: false,
        configurable: false

    });

    newUser.birthdayUser = setBirthdayUser();
    return newUser
};

const setFirstName = () => {
    let FirstName = prompt('Please enter your first name', '');
    while (FirstName === '' || FirstName == null) {
        FirstName = prompt('You did not enter your first name. Please try again', '');
    }
    return FirstName
};

const setLastName = () => {
    let LastName = prompt('Please enter your last name', '');
    while (LastName === '' || LastName == null) {
        LastName = prompt('You did not enter your last name. Please try again', '');
    }
    return LastName
};

const setBirthdayUser = () => {
    let birthdayUser = prompt('Please enter your birthday in format "dd.mm.yyyy"', '');
    while (birthdayUser === '' || birthdayUser == null) {
        birthdayUser = prompt('You did not enter your first name. Please try again', '');
    }
    return birthdayUser
};

console.log(createNewUser().getAge());
console.log(createNewUser().getPassword());


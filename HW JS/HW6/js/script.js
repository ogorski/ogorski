"use strict"

let array01 = ['hello', 'world', 11, '41', null];
let type = 'string';

const filterBy = (someArray, someType) => someArray.filter(item => typeof(item) !== someType);

console.log(filterBy(array01, type));
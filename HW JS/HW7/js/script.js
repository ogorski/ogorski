"use strict"

let array1 = ['hello', 'Kyiv', 'Lviv', 'Washington', 'world'];
let array2 = ['1', '2', '3', 'sea', array1, 41];

const showList = (item) => {
    const fragment = new DocumentFragment();
    const list = document.createElement('ul');

    item.map((val) => {
        const listItem = document.createElement('li');
        if (typeof(val) === "object" ) {
            const aditionalList = document.createElement('ul');
            val.map((a) => {
                const additionalItem = document.createElement('li');
                additionalItem.innerHTML = `${a}`;
                aditionalList.append(additionalItem);
            });
            listItem.append(aditionalList);
        } else {
            listItem.innerHTML = `${val}`;
        }
        list.append(listItem);
    });

    fragment.append(list);
    return document.body.append(fragment);
};

showList(array2);

// Таймер для чистой страницы

const showTimer = (sec) => {
    const timer = document.createElement('p');
    timer.style.cssText = `margin-top: 20px;
                           font-size: 24px;
                           text-align: center;
                           color: red;`;
    timer.innerHTML = `Page will be cleaned after: ${sec} sec`;
    document.body.append(timer);
    setInterval(() => {
        sec--;
        timer.innerHTML = `Page will be cleaned after: ${sec} sec`;
    }, 1000);

    setTimeout(() => document.body.innerHTML = '', sec*1000);
};

showTimer(60);


"use strict";

let tabsBlock = document.querySelector('.tabs'),
    tabs = document.querySelectorAll('.tabs-title'),
    tabsContent = document.querySelector('.tabs-content').children;

const hideTabContent = (a) => {
    for (let i = a; i < tabsContent.length; i++) {
        tabsContent[i].style.display = 'none';
    }
};
hideTabContent(1);


const showTabContent = (b) => {
    if(tabsContent[b].style.display === 'none') {
        tabsContent[b].style.display = 'block';
    }
};

tabsBlock.addEventListener('click', (event) => {
    if(event.target && event.target.classList.contains('tabs-title')) {
        for (let i = 0; i < tabs.length; i++) {
            tabs[i].classList.remove('active');
            if(event.target === tabs[i]) {
                tabs[i].classList.add('active');
                hideTabContent(0);
                showTabContent(i);
            }
        }
    }
});
